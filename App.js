import React from 'react';
import { StyleSheet, Text, Image, TextInput, Button, View } from 'react-native';
import Amplify, { Auth } from 'aws-amplify';
import { ConfirmSignIn, ConfirmSignUp, ForgotPassword, SignIn, SignUp, VerifyContact, withAuthenticator } from 'aws-amplify-react-native';
import {awsmobile} from './aws-exports';
import MySignIn from './app/screens/MySignIn';
import MySignUp from './app/screens/MySignUp';
import Home from './app/screens/Home';

Amplify.configure(awsmobile);

export default class App extends React.Component {
 state = {
   isAuthenticated: false
 }
 authenticate(isAuthenticated) {
   this.setState({ isAuthenticated })
 }
 render() {
   if (this.state.isAuthenticated) {
     console.log('Auth: ', Auth)
     return (
       <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
         <Text>Hello {Auth.user.username}!</Text>
       </View>
     )
   }
   return (
     <Home />
   );
 }
}

const styles = StyleSheet.create({
 container: {
   flex: 1,
   backgroundColor: '#fff',
 },
});
