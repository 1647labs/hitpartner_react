import React from 'react';
import { Stylesheet, Image } from 'react-native';

export default class Splash extends React.Component {
  render(){
  return (
    <Image style={styles.SplashScreen} />

  );
}
}

const styles = Stylesheet.create({
  container: {

  },
  "splash": {
  "image": "./assets/splash.png",
  "resizeMode": "cover"
}

});
