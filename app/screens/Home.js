import React from 'react';
import { StyleSheet, Image, View, TextInput } from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Thumbnail, Body, Left, Right } from 'native-base';
import { StackNavigator } from "react-navigation";
import Profile from './Profile';


 class Home extends React.Component {
render(){
  return(
    <Container>
      <Header />
      <Content>
        <List>
          <ListItem button onPress={() => this.props.navigation.navigate('Profile')}>
          <Thumbnail round size={80} source={require('../assets/avatar.png')} />
          <Body>
          <Text>Dejan Lovren</Text>
          <Text> Pilsen, Chicago</Text>
          <Text>4.0 | 200 reviews | UTR 8</Text>
          </Body>
          </ListItem>
          <ListItem>
          <Thumbnail round size={80} source={require('../assets/avatar.png')} />
          <Body>
          <Text>Dejan Lovren</Text>
          <Text> Pilsen, Chicago</Text>
          <Text>4.0 | 200 reviews | UTR 8</Text>
          </Body>
          </ListItem>
          <ListItem>
          <Thumbnail round size={80} source={require('../assets/avatar.png')} />
          <Body>
          <Text>Dejan Lovren</Text>
          <Text> Pilsen, Chicago</Text>
          <Text>4.0 | 200 reviews | UTR 8</Text>
          </Body>
          </ListItem>
        </List>
      </Content>
    </Container>
  )
}

}
const styles = StyleSheet.create({
  input: {
    height: 50,
    borderBottomWidth: 2,
    borderBottomColor: '#2196F3',
    margin: 10
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  bigblack: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 30,
  }
});


const Home = StackNavigator({
  Home: { screen: HomeScreen },
  Profile: { screen: ProfileScreen },
});

export default Home;
